# cocobambu-frontend

Necessário ter a aplicação "cocobambu-bff" rodando na porta 8080.

### Como rodar
```sh
$ git clone
$ cd <cocobambu-frontend>
$ npm install
$ npm start
```

### Login
```sh
email:    usuario@email.com
password: SENHA_123456
```

### Testes e2e
```sh
$ npm run cypress
```
