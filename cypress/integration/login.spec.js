const user = require('../__mocks__/user.json')

describe('/login', () => {

  it("should show and hide password", () => {
    cy.visit('http://localhost:4200/login')

    cy.get('input[type="email"]').its('length').should('eq', 1)
    cy.get('input[type="password"]').its('length').should('eq', 1)

    cy.get('input[name="email"]').type(user.email)
    cy.get('input[name="password"]').type(user.password)
    cy.get('.rightIcon').click()

    cy.get('input[type="email"]').its('length').should('eq', 1)
    cy.get('input[type="text"]').its('length').should('eq', 1)
    cy.get('.rightIcon').click()

    cy.get('input[type="email"]').its('length').should('eq', 1)
    cy.get('input[type="password"]').its('length').should('eq', 1)
  });

  it("should login", () => {
    cy.visit('http://localhost:4200/login')

    cy.get('input[name="email"]').type(user.email)
    cy.get('input[name="password"]').type(user.password)

    cy.get('.button').click()

    cy.url().should('include', '/recipes');
  });

});