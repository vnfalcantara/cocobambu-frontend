describe('/recipes', () => {

  it('should display all recipes', () => {
    cy.get('cb-recipe-card > .wrapper').should(($card) => {
      expect($card.length).to.be.greaterThan(1)
    })
  })

  it('should display one recipe', () => {
    cy.get('input[name="search"]').type('arroz')
    cy.get('cb-recipe-card > .wrapper').its('length').should('eq', 1)
  });

  it('should access the recipe page', () => {
    cy.get('.fab.primary').click()
    cy.url().should('include', '/recipe');
  });

})