import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { RecipesComponent } from './pages/recipes/recipes.component'
import { RecipeComponent } from './pages/recipe/recipe.component'
import { SplashscreenComponent } from './pages/splashscreen/splashscreen.component';

const routes: Routes = [
  { path: 'splashscreen', component: SplashscreenComponent },
  { path: 'login', component: LoginComponent },
  { path: 'recipes', component: RecipesComponent },
  { path: 'recipe/:id', component: RecipeComponent },
  { path: '',   redirectTo: '/splashscreen', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
