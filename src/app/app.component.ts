import { Component, Injectable, OnInit } from '@angular/core';
import * as Lottie from 'lottie-web'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  title = 'cocobambu-frontend';
  public lottie: any = Lottie
  public lottieConfig: Object;

  public loading: boolean = false;
  public showDialog: boolean = false;
  public dialogTitle: string;
  public dialogText: string;

  constructor() {
  }

  ngOnInit(): void {
  }

  ngAfterContentInit() {
    this.loadLottie()
  }

  loadLottie() {
    this.lottie.loadAnimation({
      container: document.querySelector('.lottie'), // the dom element that will contain the animation
      renderer: 'svg',
      loop: true,
      autoplay: true,
      path: '../../assets/animations/food.json' // the path to the animation json
    });
  }

  public isLoading() { return this.loading }
  public setLoading(loading) { this.loading = loading }

  public getShowDialog() { return this.showDialog }
  public setShowDialog(showDialog) { this.showDialog = showDialog }

  public getDialogTitle() { return this.dialogTitle }
  public setDialogTitle(dialogTitle) { this.dialogTitle = dialogTitle }

  public getDialogText() { return this.dialogText }
  public setDialogText(dialogText) { this.dialogText = dialogText }

}
