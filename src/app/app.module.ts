import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Injector } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { ReactiveFormsModule } from '@angular/forms';

import { FlexLayoutModule } from '@angular/flex-layout';
import { LoginComponent } from './pages/login/login.component';
import { RecipesComponent } from './pages/recipes/recipes.component';
import { ButtonComponent } from './components/button/button.component';
import { TextfieldComponent } from './components/textfield/textfield.component';
import { OrderDialogComponent } from './components/order-dialog/order-dialog.component';
import { RecipeCardComponent } from './components/recipe-card/recipe-card.component';
import { UserService } from './services/user.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TokenService } from './services/token.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { InterceptorService } from './services/interceptor.service';
import { RecipeService } from './services/recipe.service';
import { ScrollingModule} from '@angular/cdk/scrolling';
import { RecipeNamePipe } from './pipes/recipe.pipe';
import { RecipeComponent } from './pages/recipe/recipe.component';
import { CheckComponent } from './components/check/check.component';
import { LoadComponent } from './components/load/load.component';
import { SplashscreenComponent } from './pages/splashscreen/splashscreen.component';
import { ProgressbarComponent } from './components/progressbar/progressbar.component';
import { DialogComponent } from './components/dialog/dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RecipesComponent,
    ButtonComponent,
    TextfieldComponent,
    OrderDialogComponent,
    RecipeCardComponent,
    RecipeNamePipe,
    RecipeComponent,
    CheckComponent,
    LoadComponent,
    SplashscreenComponent,
    ProgressbarComponent,
    DialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FlexLayoutModule,
    ScrollingModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    },

    HttpClient,
    TokenService,
    LoadComponent,
    UserService,
    RecipeService
  ],

  bootstrap: [AppComponent]
})

export class AppModule {

  constructor(private injector: Injector) {
    const appButton = createCustomElement(ButtonComponent, { injector })

    customElements.define('app-button', appButton)
  }

}
