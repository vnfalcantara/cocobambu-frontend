import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'cb-check',
  templateUrl: './check.component.html',
  styleUrls: ['./check.component.scss']
})
export class CheckComponent implements OnInit {

  @Input() checked: boolean
  @Input() title: string
  @Input() description: string

  constructor() {}

  ngOnInit(): void {
  }

}
