import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'cb-order-dialog',
  templateUrl: './order-dialog.component.html',
  styleUrls: ['./order-dialog.component.scss']
})
export class OrderDialogComponent implements OnInit {

  @Input() quantity: number;
  @Input() label: string;
  @Input() description: string;

  constructor() { }

  ngOnInit(): void {
  }

}
