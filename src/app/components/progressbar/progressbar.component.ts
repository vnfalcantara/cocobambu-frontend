import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'cb-progressbar',
  templateUrl: './progressbar.component.html',
  styleUrls: ['./progressbar.component.scss']
})
export class ProgressbarComponent implements OnInit {

  @Input() progress: string
  @Input() width: string
  @Input() height: string

  constructor() { }

  ngOnInit(): void {
  }

}
