import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'cb-recipe-card',
  templateUrl: './recipe-card.component.html',
  styleUrls: ['./recipe-card.component.scss']
})

export class RecipeCardComponent implements OnInit {

  @Input() img: string;
  @Input() title: string;
  @Input() description: string;
  @Input() status: string;
  @Input() when: string;
  @Input() hour: string;
  
  @Output() viewMoreClick = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onViewMoreClick(event) {
    this.viewMoreClick.emit(event)
  }

}
