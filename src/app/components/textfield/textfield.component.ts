import { Component, OnInit, Input, Output, EventEmitter, Injector } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ControlValueAccessorService } from '../../services/controlvalueaccessor.service';

@Component({
  selector: 'cb-textfield',
  templateUrl: './textfield.component.html',
  styleUrls: ['./textfield.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: TextfieldComponent, multi: true
  }]
})

export class TextfieldComponent extends ControlValueAccessorService implements OnInit {

  @Input() formControlName: string;
  @Input() name: string;
  @Input() value: string;
  @Input() type: string;
  @Input() leftIcon: string;
  @Input() leftIconPointer: boolean;
  @Input() rightIcon: string;
  @Input() rightIconPointer: boolean;
  @Input() placeholder: string;
  @Input() errorMessage: string;

  @Output() rightIconClick = new EventEmitter();
  @Output() valueChange = new EventEmitter<string>(); 

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
  }

  clearInput() {
    this.control.setValue('');
  }

  emitRightIconClick(event) {
    this.rightIconClick.emit(event)
  }
  
  updateValue(value) {
    this.value = value;
    this.valueChange.emit(value);
  }

}
