import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ValidationService } from '../../services/validation.service'
import { UserService } from '../../services/user.service'
import { TokenService } from '../../services/token.service'
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  ordersLength = 1
  visible: boolean = false
  iconVisible: string = '../../../assets/visibility_off.svg'
  iconInvisible: string = '../../../assets/visibility_on.svg'
  visibilityIcon: string = this.iconVisible
  passwordFieldType: string = 'password'
  loginForm: FormGroup;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private tokenService: TokenService,
    private app: AppComponent
  ) {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, ValidationService.email]],
      password: ['', [Validators.required, Validators.minLength(3)]],
    });
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    setTimeout(() => this.app.setLoading(false), 0) // ExpressionChangedAfterItHasBeenCheckedError
  }

  toggleVisibility() {
    this.visible = !this.visible;
    this.visibilityIcon = this.visible ? this.iconInvisible : this.iconVisible;
    this.passwordFieldType = this.visible ? 'text' : 'password'
  }

  login() {
    const { email, password } = this.loginForm.value

    if (this.loginForm.valid) {

      this.app.setLoading(true)

      this.userService.login(email, password).subscribe(
        data => this.loginSuccess(data),
        error => { debugger }
      )

    }

  }

  loginSuccess(data) {
    this.tokenService.set(data.token)
    this.router.navigate(['/recipes', {}]);
  }

}
