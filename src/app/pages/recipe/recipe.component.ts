import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RecipeService } from 'src/app/services/recipe.service';
import { environment } from '../../../environments/environment';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.scss']
})
export class RecipeComponent implements OnInit {

  recipe: any // TODO: INTERFACE
  progress: number = 0
  minutes: number = 0

  ingredientsComplete: boolean = false
  started: boolean = false
  finished: boolean = false

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private recipeService: RecipeService,
    private app: AppComponent
  ) {
    this.activatedRoute.params.subscribe(params => {
      this.getRecipe(params.id)
    })
  }

  ngOnInit(): void {
    this.listeners()
  }

  getRecipe(id) {
    this.app.setLoading(true)

    this.recipeService.findById(id).subscribe(
      data => this.setRecipe(data),
      error => { debugger }
    )
  }

  setRecipe(recipe) {
    recipe.img = `${environment.API_HOST}/${recipe.img}`
    this.recipe = recipe
    this.app.setLoading(false)
  }

  listeners() {
    const content = document.getElementById('content')

    content.onscroll = event => {
      const header = document.getElementById('header')
      const title = document.getElementById('title')

      if (content.scrollTop > 5 || content.scrollTop > 5) {
        header.style.height = '200px'
        title.style.fontSize = '24px'
      } else {
        header.style.height = '400px'
        title.style.fontSize = '40px'
      }
    };
  }

  back() {
    this.router.navigate(['/recipes']);
  }

  toggleIngredient(i) {
    this.recipe.ingredients[i].checked = !this.recipe.ingredients[i].checked
  }

  toggleStep(i) {
    if (this.started) {
      this.recipe.preparation.steps[i].completed = !this.recipe.preparation.steps[i].completed
      this.progress = this.calcProcess()
    }
  }

  startPreparation() {
    this.started = true
  }

  calcProcess() {
    const max = this.recipe.preparation.steps.length
    const current = this.recipe.preparation.steps.filter(d => d.completed).length

    return current * 100 / max
  }

  finishDialog() {
    //orderedAt - finishedAt
    this.app.setDialogTitle('OBRIGADO')
    this.app.setDialogText('Prato finalizado com sucesso em x minutos e y segundos')
    this.app.setShowDialog(true)
  }

}
