import { Component, OnInit } from '@angular/core';
import { RecipeService } from '../../services/recipe.service'
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { AppComponent } from 'src/app/app.component';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.scss']
})
export class RecipesComponent implements OnInit {

  search: string = ''
  recipes: Array<any> = [] // TODO: CRIAR INTERFACE

  constructor(
    private router: Router,
    private recipeService: RecipeService,
    private userService: UserService,
    private app: AppComponent
  ) {
  }

  ngOnInit(): void {
    
  }

  ngAfterViewInit(): void {
    setTimeout(() => this.getRecipes(), 0) // ExpressionChangedAfterItHasBeenCheckedError
  }

  getRecipes() {
    const query = {}
    const fields = { name: 1, description: 1, thumbnail: 1, orderedAt: 1, status: 1 }

    this.app.setLoading(true)

    this.recipeService.find(query, fields).subscribe(
      data => this.setRecipes(data),
      error => { debugger }
    )
  }

  setRecipes(recipes) {
    recipes.map(recipe => recipe.thumbnail = `${environment.API_HOST}/${recipe.thumbnail}` )
    this.recipes = recipes
    this.app.setLoading(false)
  }

  viewMore(id) {
    this.router.navigate([`/recipe/${id}`]);
  }

  logout() {
    this.userService.logout()
    this.router.navigate(['/login']);
  }

}
