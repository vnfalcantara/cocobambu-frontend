import { Component, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-splashscreen',
  templateUrl: './splashscreen.component.html',
  styleUrls: ['./splashscreen.component.scss']
})
export class SplashscreenComponent implements OnInit {

  constructor(
    public app: AppComponent,
    private router: Router,
    private userSerice: UserService
  ) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    setTimeout(() => this.app.setLoading(true), 0) // ExpressionChangedAfterItHasBeenCheckedError
    setTimeout(() => { this.checkToken() }, 2000) // SIMULAR CARREGAMENTO
  }

  checkToken() {
    this.userSerice.me().subscribe(
      data => this.checkTokenSuccess(data),
      error => this.goTo('/login')
    )
  }

  checkTokenSuccess(data) {
     if (!data) this.goTo('/login')
     else this.goTo('/recipes')
  }

  goTo(route) {
    this.router.navigate([route, {}]);
  }

}
