import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'recipeName'
})
export class RecipeNamePipe implements PipeTransform {

  transform(recipes: any, name: any): any {
    if (!name) return recipes;

    return recipes.filter(recipe => {
      return recipe.name.toLowerCase().includes(name.toLowerCase()); 
    });
  };

}