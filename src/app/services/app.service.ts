import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import * as qs from 'qs';

export class AppService {

  url: string
  http: HttpClient

  constructor( path, http ) {
    this.url = `${environment.API_HOST}/${path}`
    this.http = http
  }

  insert() { }

  find(params = {}, fields = {}) {
    const querystring = qs.stringify({ params, fields }, { encode: false })
    return this.http.get(`${this.url}?${querystring}`)
  }

  findById(id = '', fields = {}) {
    const querystring = qs.stringify({ fields }, { encode: false })
    return this.http.get(`${this.url}/${id}?${querystring}`)
  }

  count() { }

  update() { }

  remover() { }

}