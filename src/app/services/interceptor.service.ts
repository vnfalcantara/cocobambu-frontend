import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenService } from './token.service'

@Injectable()
export class InterceptorService implements HttpInterceptor {

  auth: string

  constructor(private tokenService: TokenService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    this.auth = this.tokenService.get()

    if (this.auth) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${this.auth}`
        }
      })
    }

    return next.handle(request);

  }

}