import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppService } from '../services/app.service'
import { environment } from '../../environments/environment';

@Injectable()
export class RecipeService extends AppService {

  url: string

  constructor(public http: HttpClient) {
    super('recipe', http);
    this.url = `${environment.API_HOST}/recipe`;
  }

}