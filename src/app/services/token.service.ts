export class TokenService {

  get() {
    return localStorage.getItem('auth')
  }

  set(auth) {
    return localStorage.setItem('auth', auth)
  }

  destroy() {
    return localStorage.removeItem('auth')
  }

}